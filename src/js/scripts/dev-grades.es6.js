var React = window.React = require("react");
var request = require("-aek/request");
var { Listview, Item } = require("-components/listview");
var AekStorage = require("-aek/storage");
var storage = new AekStorage("aek2-dev-grades");
var gradesList = [];


class DevGrades{
  fetchGrades(cb) {
    var rawResponse;
    request.action('get-grades').end((err, response) => {
      console.log("ERR: " + err + " Response3333: " + JSON.stringify(response) );
      rawResponse = JSON.stringify(response);

      var body = response !== undefined ? response.body : null;
      if(body !== null){
        var grades = JSON.parse(body);
        grades.map((item) => {

          gradesList.push(
            <Item key={'grades-' + item.className + item.grade}>
              <div>{ item.className }</div>
              <div>{ item.grade }</div>
            </Item>
          );
        });
        cb(false, { grades:body});
      }
      else{
        gradesList.push(
          <Item key={'grades-00'}>
            aaaaaa
            <div>{rawResponse}</div>
          </Item>
        );
        //console.log("gradesList: " + JSON.stringify(gradesList));
        cb(true, 'body is null');
      }
    storage.set("gradesList", gradesList)
    });
console.log("gradesList222: " + gradesList);
  }
}
module.exports = new DevGrades();
