let React = window.React = require("react");
let request = require("-aek/request");
let reactRender = require("-aek/react/utils/react-render");
let Page = require("-components/page");
let { Listview, Item } = require("-components/listview");
let Container = require("-components/container");
let {VBox, Panel} = require("-components/layout");
let {BannerHeader} = require("-components/header");
let {BasicSegment} = require("-components/segment");
let gradesCSS = require("../css/grades");
let AekStorage = require("-aek/storage");
let storage = new AekStorage("aek2-dev-grades");
let DevGrades = require("./scripts/dev-grades");
let gradesList = [];


let Screen = React.createClass({

  componentWillMount:function(){

    DevGrades.fetchGrades((err, response) => {
       console.log("ERR: " + err + " Response: " + JSON.stringify(response) );
    });
  },

  render:function() {
    //gradesList = storage.get('gradesList');
    let list =
      <BasicSegment nopadding >
        <Listview className="resultsList" style={{ marginTop:'1rem', borderRadius:'0' }}>{ gradesList }</Listview>
      </BasicSegment>
    return (
      <Page>
         <VBox>
            <BannerHeader  key='index-header' data-flex={0}>Heading</BannerHeader>
            <Panel>
              <BasicSegment>

                <div>Grades raw response: </div>
                <table class="ui table">
                  <thead>
                    <tr>
                      <th>class</th>
                      <th>Grade</th>
                    </tr>
                  </thead>
                  </table>
                {list}
              </BasicSegment>
            </Panel>
          </VBox>
      </Page>

    );

  }

});

reactRender(<Screen/>);
